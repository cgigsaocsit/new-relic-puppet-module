New Relic
================================
A Puppet module for deploying and configuring New Relic monitoring.

#### Table of Contents
* [Usage](https://bitbucket.org/cgigsaocsit/new-relic-puppet-module/overview#markdown-header-usage)
	* [Server Monitoring](https://bitbucket.org/cgigsaocsit/new-relic-puppet-module/overview#markdown-header-server-monitoring)

	* [APM](https://bitbucket.org/cgigsaocsit/new-relic-puppet-module/overview#markdown-header-apm)
		* [PHP](https://bitbucket.org/cgigsaocsit/new-relic-puppet-module/overview#markdown-header-php)

- - -

Usage
================================
### Server Monitoring ###

```
#!puppet

class { 'new_relic::servers':
    license_key     => 'NEW_RELIC_LICENSE_KEY',
    labels     		=> [
    	"Datacenter:Primary",
    	"Environment:Production",
    ],
}
```

### APM ###
#### PHP ####
```
#!puppet

class { 'new_relic::apm::php':
    license_key     		=> 'NEW_RELIC_LICENSE_KEY',
    default_php_application => "Default PHP Application"
	high_security			=> true,
}
```
**This should be used in conjunction with one of the configuration options below**

* **Puppet**: new_relic_app_name => 'sites.usa.gov' parameter in the ocsit_webserver::virtualhost::http and ocsit_webserver::virtualhost::ssl Puppet resources
* **Hardcoded Apache VHost Directive**: php_value newrelic.appname "sites.usa.gov"
