class new_relic::servers::redhat::configuration (
  $ensure             = present,
  $license_key,
  $labels,
  $owner              = $new_relic::servers::redhat::params::user,
  $group              = $new_relic::servers::redhat::params::group,
  $mode               = $new_relic::servers::redhat::params::mode,
  $nrsysmond_path     = $new_relic::servers::redhat::params::nrsysmond_path,
  $nrsysmond_content  = template('new_relic/servers/redhat/nrsysmond.cfg.erb'),
  $nrsysmond_require  = [Package[$new_relic::servers::redhat::params::newrelic_sysmond_package]],
  $nrsysmond_notify   = [Service[$new_relic::servers::redhat::params::newrelic_sysmond_service]],
) inherits new_relic::servers::redhat::params {

	file { $nrsysmond_path:
		ensure  => $ensure,
		owner	  => $owner,
		group	  => $group,
		mode    => $mode,
		content	=> $nrsysmond_content,
		require	=> $nrsysmond_require,
		notify	=> $nrsysmond_notify,
	}
}
