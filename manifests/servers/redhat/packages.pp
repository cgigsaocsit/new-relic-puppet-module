class new_relic::servers::redhat::packages (
  $ensure                   = installed,
  $newrelic_sysmond_package = $new_relic::servers::redhat::params::newrelic_sysmond_package,
) inherits new_relic::servers::redhat::params {

  $_ensure = $ensure ? {
    present => installed,
    default => $ensure,
  }

	package { $newrelic_sysmond_package:
		ensure	=> $_ensure,
	}
}
