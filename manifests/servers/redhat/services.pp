class new_relic::servers::redhat::services (
  $ensure = running,
  $enable = true,
  $newrelic_sysmond_service = $new_relic::servers::redhat::params::newrelic_sysmond_service,
  $newrelic_sysmond_require = [Package[$new_relic::servers::redhat::params::newrelic_sysmond_package]],
) inherits new_relic::servers::redhat::params{

  $_ensure = $ensure ? {
    present => running,
    absent  => stopped,
    default => $ensure,
  }

	service { $newrelic_sysmond_service:
		ensure	=> $_ensure,
		enable	=> $enable,
		require	=> $newrelic_sysmond_require,
	}
}
