class new_relic::servers::redhat::params {
	$nrsysmond_path	= '/etc/newrelic/nrsysmond.cfg'
	$user            = 'root'
	$group           = 'newrelic'
	$mode            = '640'

  $newrelic_sysmond_service = 'newrelic-sysmond'
  $newrelic_sysmond_package = 'newrelic-sysmond'
}
