class new_relic::servers::redhat (
  $ensure       = present,
  $license_key,
  $labels
) {
	include new_relic::servers::redhat::params

	class { 'new_relic::servers::redhat::packages':
    ensure      => $ensure,
	}
	class { 'new_relic::servers::redhat::configuration':
    ensure      => $ensure,
		license_key	=> $license_key,
		labels		  => $labels,
	}
	class { 'new_relic::servers::redhat::services':
    ensure      => $ensure,
	}
}
