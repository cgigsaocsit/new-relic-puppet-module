class new_relic::servers::windows::params {
	$newrelic_sysmond_msi_path     = "c:\installers\NewRelicServerMonitor_${::architecture}.msi"
	$source_msi_path   = "puppet:///modules/new_relic/servers/windows/NewRelicServerMonitor_${::architecture}.msi"
	$install_log       = "c:\installers\NewRelicServerMonitor_${::architecture}.msi_install.log"

	$newrelic_sysmond_package		= 'New Relic Server Monitor'
	$newrelic_sysmond_service		= 'nrsvrmon'

	$registry_host               = 'HKEY_LOCAL_MACHINE\SOFTWARE\New Relic\Server Monitor\Host'
	$nrsysmond_license_reg_path  = 'HKEY_LOCAL_MACHINE\SOFTWARE\New Relic\Server Monitor\LicenseKey'
}
