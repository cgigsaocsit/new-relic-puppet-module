class new_relic::servers::windows::services (
  $ensure = running,
  $newrelic_sysmond_service = $new_relic::servers::windows::params::newrelic_sysmond_service,
  $newrelic_sysmond_require = [Package[$new_relic::servers::windows::params::newrelic_sysmond_package]],
) inherits new_relic::servers::windows::params {

  $_ensure = $ensure ? {
    present => running,
    absent  => stopped,
    default => $ensure,
  }

	service { $newrelic_sysmond_service:
		ensure	=> $_ensure,
		enable	=> true,
		require	=> $newrelic_sysmond_require,
	}
}
