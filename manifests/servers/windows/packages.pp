class new_relic::servers::windows::packages (
  $ensure                         = installed,
  $license_key,
  $newrelic_sysmond_package       = $new_relic::servers::windows::params::newrelic_sysmond_package,
  $newrelic_sysmond_msi_path      = $new_relic::servers::windows::params::newrelic_sysmond_msi_path,
  $newrelic_sysmond_source_path   = $new_relic::servers::windows::params::source_msi_path,
  $newrelic_sysmond_install_log   = $new_relic::servers::windows::params::install_log,
) inherits new_relic::servers::windows::params {

  $_ensure = $ensure ? {
    present => installed,
    default => $ensure,
  }

	package { $newrelic_sysmond_package:
		ensure          => $_ensure,
		provider		    => 'windows',
		source          => $newrelic_sysmond_msi_path,
		install_options => [
			'/l', $newrelic_sysmond_install_log,
             {"NR_LICENSE_KEY" => "\"${license_key}\""}
		],
		require			    => File[$newrelic_sysmond_msi_path],
	}

	file { $newrelic_sysmond_msi_path:
		ensure	=> present,
		source	=> $newrelic_sysmond_source_path,
		links	  => follow,
	}

	if ! defined(Package['ffi']) {
		package { 'ffi':
			ensure		=> 'installed',
			provider	=> 'gem'
		}
	}
}
