class new_relic::servers::windows::configuration (
  $ensure                         = present,
  $license_key,
  $nrsysmond_license_reg_path     = $new_relic::servers::windows::params::nrsysmond_license_reg_path,
  $nrsysmond_license_reg_require  = [Package[$new_relic::servers::windows::params::newrelic_sysmond_package]],
  $nrsysmond_license_reg_notify   = [Service[$new_relic::servers::windows::params::newrelic_sysmond_service]],
) inherits new_relic::servers::windows::params {

	registry_value { $nrsysmond_license_reg_path:
  		ensure  => $ensure,
  		type    => string,
  		data    => $license_key,
  		require	=> $nrsysmond_license_reg_require,
  		notify	=> $nrsysmond_license_reg_notify,
	}
}
