class new_relic::servers::windows (
  $ensure       = present,
  $license_key
) {
	class { 'new_relic::servers::windows::packages':
    ensure      => $ensure,
		license_key	=> $license_key
	}
	class { 'new_relic::servers::windows::configuration':
    ensure      => $ensure,
		license_key	=> $license_key
	}
	class { 'new_relic::servers::windows::services':
    ensure      => $ensure,
	}
}
