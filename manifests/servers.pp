class new_relic::servers (
  $ensure       = present,
  $license_key,
  $labels       = undef
) {
	case $::operatingsystem {
		'RedHat', 'CentOS': {
			class { 'new_relic::servers::redhat':
				ensure      => $ensure,
				license_key => $license_key,
				labels      => $labels,
			}
		}
		'windows': {
			class { 'new_relic::servers::windows':
        ensure      => $ensure,
				license_key	=> $license_key,
			}
		}
		default: {
			fail("The OS ${::operatingsystem} is not supported!")
		}
	}
}
