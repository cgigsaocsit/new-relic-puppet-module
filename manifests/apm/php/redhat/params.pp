class new_relic::apm::php::redhat::params {
	$newrelic_php_ini	= '/etc/php.d/newrelic.ini'
  $user             = 'root'
  $group            = 'root'
  $mode             = '644'

  $newrelic_php5_package  = 'newrelic-php5'

  $newrelic_install_command = '/usr/bin/newrelic-install install >> /var/log/newrelic-install.log 2>&1'
}
