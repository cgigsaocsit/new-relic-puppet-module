class new_relic::apm::php::redhat::packages (
  $ensure                   = installed,
  $license_key,
  $newrelic_php5_package    = $new_relic::apm::php::redhat::params::newrelic_php5_package,
  $newrelic_install_command = $new_relic::apm::php::redhat::params::newrelic_install_command
) inherits new_relic::apm::php::redhat::params {

  $_ensure = $ensure ? {
    present => installed,
    default => $ensure,
  }

	package { $newrelic_php5_package:
		ensure	=> $_ensure,
	} ~>
	exec { 'new_relic_install':
		command     => $newrelic_install_command,
		environment	=> ['NR_INSTALL_SILENT=true', "NR_INSTALL_KEY=${license_key}"],
		path		    => '/usr/kerberos/sbin:/usr/kerberos/bin:/usr/bin:/bin:/usr/java/latest/bin:/sbin:/usr/sbin:/usr/local/bin:/usr/local/rvm/bin',
		refreshonly	=> true,
	}
}
