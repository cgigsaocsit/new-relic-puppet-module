class new_relic::apm::php::redhat::configuration (
  $ensure                     = present,
	$license_key,
	$default_php_application,
	$high_security,
  $new_relic_labels           = [],
  $new_relic_php_ini          = $new_relic::apm::php::redhat::params::newrelic_php_ini,
  $enw_relic_php_ini_conetent = template('new_relic/apm/php/redhat/newrelic.ini.erb'),
  $owner                      = $new_relic::apm::php::redhat::params::user,
  $group                      = $new_relic::apm::php::redhat::params::group,
  $mode                       = $new_relic::apm::php::redhat::params::mode,
) inherits new_relic::apm::php::redhat::params {

	file { $new_relic_php_ini:
		ensure	=> $ensure,
		owner	  => $owner,
		group	  => $group,
		mode    => $mode,
		content	=> $enw_relic_php_ini_conetent,
	}
}
