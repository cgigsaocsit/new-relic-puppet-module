class new_relic::apm::php::redhat (
  $ensure                     = present,
	$license_key,
	$default_php_application,
	$high_security,
  $new_relic_labels           = [],
) inherits new_relic::apm::php::redhat::params {

	class { 'new_relic::apm::php::redhat::packages':
	  ensure      => $ensure,
		license_key	=> $license_key
	}
	class { 'new_relic::apm::php::redhat::configuration':
    ensure                  => $ensure,
		license_key             => $license_key,
		default_php_application	=> $default_php_application,
		high_security           => $high_security,
		new_relic_labels        => $new_relic_labels,
	}
}
