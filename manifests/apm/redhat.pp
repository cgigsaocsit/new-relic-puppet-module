class new_relic::apm::redhat (
  $ensure = present,
) inherits new_relic::apm::php::redhat::params {

	class { 'new_relic::apm::redhat::packages':
    ensure => $ensure,
	} ->
	class { 'new_relic::apm::redhat::configuration':
    ensure => $ensure,
	}
}
