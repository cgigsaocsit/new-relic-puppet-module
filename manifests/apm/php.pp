class new_relic::apm::php (
  $ensure                    = present,
	$license_key,
	$default_php_application   = 'PHP Application',
	$high_security             = false,
	$new_relic_labels          = [],
) {

	if ! defined(Class['new_relic::apm']) {
		class { 'new_relic::apm':
      ensure => $ensure,
		}
	}

	case $::operatingsystem {
		'RedHat', 'CentOS': {
			class { 'new_relic::apm::php::redhat':
        ensure                  => $ensure,
				license_key				      => $license_key,
				default_php_application	=> $default_php_application,
				high_security			      => $high_security,
				new_relic_labels		    => $new_relic_labels,
				require					        => Class['new_relic::apm'],
			}
		}
		default: {
			fail("The OS ${::operatingsystem} is not supported!")
		}
	}
}
