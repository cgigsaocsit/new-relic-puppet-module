class new_relic::apm::redhat::packages (
  $ensure                     = installed,
  $license_key,
  $newrelic_daemon_package    = $new_relic::apm::redhat::params::newrelic_daemon_package,
) inherits new_relic::apm::redhat::params {

  $_ensure = $ensure ? {
    present => installed,
    default => $ensure,
  }

	package { $newrelic_daemon_package:
		ensure	=> $_ensure,
	}
}
