class new_relic::apm::redhat::configuration (
  $ensure                   = present,
  $allow_ypbind_persistent  = true,
) inherits new_relic::apm::redhat::params {

	if ($::selinux == 'true') {
		  $allow_ypbind_value = $ensure ? {
		   present  => on,
		   absent   => off,
		   default  => off,
		  }

	  	selboolean { 'allow_ypbind':
	  		persistent	=> $allow_ypbind_persistent,
	  		value		    => $allow_ypbind_value,
	  	}
  	}
}
