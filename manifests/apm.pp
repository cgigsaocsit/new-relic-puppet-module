class new_relic::apm (
  $ensure = present,
) {
	case $::operatingsystem {
		'RedHat', 'CentOS': {
			class { 'new_relic::apm::redhat':
			  ensure => $ensure,
			}
		}
		default: {
			fail("The OS ${::operatingsystem} is not supported!")
		}
	}
}
